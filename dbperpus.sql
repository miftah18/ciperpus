-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2018 at 10:58 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbperpus`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `nama`, `username`, `password`) VALUES
(1, 'miftah', 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `kdanggota` char(5) NOT NULL,
  `nama_anggota` varchar(35) NOT NULL,
  `nohp` varchar(15) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`kdanggota`, `nama_anggota`, `nohp`, `alamat`) VALUES
('A0001', 'M. Nasirudin Karim', '081987654321', 'Jalan. Melati No 20 Mataram'),
('A0002', 'Khaeril Anwar', '08198765432', 'Jalan Rusunawa No 2 Mataram');

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `kdbuku` char(5) NOT NULL,
  `judulbuku` varchar(50) NOT NULL,
  `pengarang` varchar(50) NOT NULL,
  `penerbit` varchar(50) NOT NULL,
  `tahunterbit` char(4) NOT NULL,
  `idkategori` int(3) NOT NULL,
  `stok` int(3) NOT NULL,
  `tglmasuk` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`kdbuku`, `judulbuku`, `pengarang`, `penerbit`, `tahunterbit`, `idkategori`, `stok`, `tglmasuk`) VALUES
('B0001', 'Belajar PHP dan MySQL', 'Khaerul Patoni', 'Andi Offset', '2017', 1, 0, '2017-11-07'),
('B0002', 'Sejarah Islam Indonesia', 'Muhammad Jaelani', 'Gramedia', '2015', 3, 12, '2017-11-02'),
('B0003', 'Belajar Matematika Dasar', 'Baiq Putri Wahyuni', 'Lokomedia', '2016', 1, 5, '2017-11-09'),
('B0004', 'Pengantar Ilmu Ekonomi', 'Wenti Ayu', 'Andi Offset', '2014', 2, 5, '2017-11-09');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `idkategori` int(3) NOT NULL,
  `namakategori` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`idkategori`, `namakategori`) VALUES
(1, 'Buku Komputer'),
(2, 'Buku Ekonomi'),
(3, 'Buku Agama'),
(4, 'Buku Bahasa'),
(5, 'Buku Filsafat');

-- --------------------------------------------------------

--
-- Table structure for table `pinjam`
--

CREATE TABLE `pinjam` (
  `idpinjam` int(7) NOT NULL,
  `kdanggota` char(5) NOT NULL,
  `kdbuku` char(5) NOT NULL,
  `tglpinjam` date NOT NULL,
  `statuspinjam` varchar(7) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pinjam`
--

INSERT INTO `pinjam` (`idpinjam`, `kdanggota`, `kdbuku`, `tglpinjam`, `statuspinjam`) VALUES
(14, 'A0002', 'B0001', '2017-11-15', 'pinjam'),
(15, 'A0002', 'B0002', '2017-11-15', 'pinjam'),
(20, 'A0001', 'B0002', '2017-11-15', 'pinjam'),
(21, 'A0001', 'B0003', '2017-11-15', 'pinjam'),
(22, 'A0001', 'B0001', '2017-11-15', 'pinjam');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`kdanggota`);

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`kdbuku`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`idkategori`);

--
-- Indexes for table `pinjam`
--
ALTER TABLE `pinjam`
  ADD PRIMARY KEY (`idpinjam`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `idkategori` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pinjam`
--
ALTER TABLE `pinjam`
  MODIFY `idpinjam` int(7) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
