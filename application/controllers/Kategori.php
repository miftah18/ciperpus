<?php

class Kategori extends CI_Controller {
    function __construct() {
        parent::__construct();
        //untuk load model kategori
        $this->load->model('Kategori_model','kategori');
    }
    
    function index(){
        $this->load->view('template',[
            'judul' => 'Data Kategori',
            'content' => $this->load->view('kategori_list',[
                'kategori' => $this->kategori->get()
            ],TRUE)
        ]);
    }
    
    function form(){
        $this->load->view('template',[
            'judul' => 'Data Kategori',
            'content' => $this->load->view('kategori_form','',TRUE)
        ]);
    }
    
    function save(){
        $data = $this->input->post();
        $this->kategori->save($data);
        redirect('kategori');
    }
    function edit($idkategori){
        $this->load->view('template',[
             'judul' => 'Edit Data Kategori',
            'content' => $this->load->view('kategori_form',[
                'data' => $this->kategori->find($idkategori)
            ],true)
        ]);
    }
    function update(){
        $idkategori = $this->input->post('idkategori');
        $this->kategori->update($idkategori,$this->input->post());
        redirect('kategori');
    }
    function delete($idkategori){
        $this->kategori->delete($idkategori);
        redirect('kategori');
    }
}
