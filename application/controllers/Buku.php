<?php

class Buku extends CI_Controller {
    function __construct() {
        parent::__construct();
        //untuk load model buku
        $this->load->model('Buku_model','buku');
    }
    
    function index(){
        $this->load->model('Kategori_model','kategori');
       // $this->load->model('Makul_model','makul');
        $this->load->view('template',[
            'judul' => 'Data Buku',
            'content' => $this->load->view('buku_list',[
                'buku' => $this->buku->get(),
                'kategori' => $this->kategori->get(),
                //'makul' => $this->makul->get()
            ],TRUE)
        ]);
    }
    
    function form(){
        $this->load->model('Kategori_model','kategori');
        //$this->load->model('Makul_model','makul');
        $this->load->view('template',[
            'judul' => 'Data Buku',
            'content' => $this->load->view('buku_form',[
                'kategori' => $this->kategori->get(),
               // 'makul' => $this->makul->get()
            ],TRUE)
        ]);
    }
    
    function save(){
        $data = $this->input->post();
        $this->buku->save($data);
        redirect('buku');
    }
    function edit($kdbuku){
		$this->load->model('Kategori_model','kategori');
        //$this->load->model('Makul_model','makul');
        $this->load->view('template',[
             'judul' => 'Edit Buku',
            'content' => $this->load->view('buku_form',[
				'kategori' => $this->kategori->get(),
                //'makul' => $this->makul->get(),
                'data' => $this->buku->find($kdbuku)
            ],true)
        ]);
    }
    function update(){
        $kdanggota = $this->input->post('kdbuku');
        $this->buku->update($kdbuku,$this->input->post());
        redirect('buku');
    }
    function delete($kdbuku){
        $this->buku->delete($kdbuku);
        redirect('buku');
    }
}
