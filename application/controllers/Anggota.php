<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {
    function __construct() {
        parent::__construct();
        //untuk load model anggota
        $this->load->model('Anggota_model','anggota');
    }
    function index(){
        $this->load->view('template',[
            'judul' => 'Data Anggota',
            'content' => $this->load->view('anggota_list',[
                'anggota' => $this->anggota->get()
            ],TRUE)
        ]);
    }
    function form(){
        $this->load->view('template',[
            'judul' => 'Data Anggota',
            'content' => $this->load->view('anggota_form','',TRUE)
        ]);
    }
    function save(){
        $data = $this->input->post();
        $this->anggota->save($data);
        redirect('anggota');
    }
    function edit($kdanggota){
        $this->load->view('template',[
             'judul' => 'Edit Data Anggota',
            'content' => $this->load->view('anggota_form',[
                'data' => $this->anggota->find($kdanggota)
            ],true)
        ]);
    }
    function update(){
        $kdanggota = $this->input->post('kdanggota');
        $this->anggota->update($kdanggota,$this->input->post());
        redirect('anggota');
    }
    function delete($kdanggota){
        $this->anggota->delete($kdanggota);
        redirect('anggota');
    }
}
