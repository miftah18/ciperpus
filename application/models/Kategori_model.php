<?php

class Kategori_model extends CI_Model{
    function get(){
        return $this->db->get('kategori')->result_array();
        //optional manggil db
        //return $this->db->query('select * from kategori')->resul_array();
    }
    function save($data){
        $this->db->insert('kategori',$data);
    }
    function find($idkategori){
        return $this->db->where(['idkategori' => $idkategori])->get('kategori')->row_array();
    }
    function update($idkategori,$data){
        $this->db->where(['idkategori' => $idkategori])->update('kategori',$data);
    }
    function delete($idkategori){
        $this->db->where(['idkategori' => $idkategori])->delete('kategori');
    }
}
