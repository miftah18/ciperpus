<?php

class Anggota_model extends CI_Model{
    function get(){
        return $this->db->get('anggota')->result_array();
        //optional manggil db
        //return $this->db->query('select * from anggota')->resul_array();
    }
    function save($data){
        $this->db->insert('anggota',$data);
    }
    function find($kdanggota){
        return $this->db->where(['kdanggota' => $kdanggota])->get('anggota')->row_array();
    }
    function update($kdanggota,$data){
        $this->db->where(['kdanggota' => $kdanggota])->update('anggota',$data);
    }
    function delete($kdanggota){
        $this->db->where(['kdanggota' => $kdanggota])->delete('anggota');
    }
}
