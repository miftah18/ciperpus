<?php

class Buku_model extends CI_Model{
    function get(){
        //query join 3 table
        $sql = "SELECT b.*,
                       k.nama AS kategori
                FROM
                       buku b,
                       kategori k
                WHERE
                       b.idkategori=k.idkategori AND
                       
        
        return $this->db->query($sql)->result_array();
        
        //return $this->db->get('buku')->result_array();
        //optional manggil db
        //return $this->db->query('select * from anggota')->resul_array();
    }
    
    function save($data){
        $this->db->insert('buku',$data);
    }
    function find($kdbuku){
        return $this->db->where(['kdbuku' => $kdbuku])->get('buku')->row_array();
    }
    function update($kdbuku,$data){
        $this->db->where(['kdbuku' => $kdbuku])->update('buku',$data);
    }
    function delete($kdbuku){
        $this->db->where(['kdbuku' => $kdbuku])->delete('buku');
    }
}
