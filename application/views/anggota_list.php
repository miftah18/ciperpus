<a href="<?= site_url() ?>/anggota/form" class="btn btn-success"><span class="glyphicon glyphicon-plus"> Tambah Data</span></a><br /><br />
<table class="table table-striped table-bordered table-hover table-condensed">
    <thead style="background-color: #428bca">
        <tr style="color: white">
            <th><center>No</center></th>
			<th><center>Kode Anggota</center></th>
			<th><center>Nama</center></th>
			<th><center>No. Handphone</center></th>
			<th><center>Alamat</center></th>
			<th><center>Action</center></th>
		</tr>
	</thead>
<tbody>
    <?php $no = 0;
    foreach ($anggota as $r) {
        ?>
        <tr>
            <td align="center"><?= ++$no ?></td>
            <td align="center"><?= $r['kdanggota'] ?></td>
            <td><?= $r['nama_anggota'] ?></td>
            <td align="center"><?= $r['nohp'] ?></td>
			<td  align="center"><?= $r['alamat'] ?></td>
            <td align="center">
                <a href="<?= site_url('anggota/edit/' . $r['kdanggota']) ?>" class="btn btn-primary"><span class="glyphicon glyphicon-edit" title="Edit Data"></span></a>
                <a href="#" rel="<?= site_url('anggota/delete/' . $r['nim']) ?>" class="del btn btn-danger"><span class="glyphicon glyphicon-trash" title="Hapus Data"></span></a>
            </td>
        </tr>
<?php } ?>
</tbody>
</table>