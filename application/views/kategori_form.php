<form class="form-horizontal" method="post" action="<?= site_url() ?>/kategori/<?= isset($data['idkategori']) ? 'update' : 'save' ?>">
    <div class="form-group">
        <label class="col-sm-2 control-label">ID KATEGORI</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="idkategori" placeholder="Masukkan ID KATEGORI Anda" value="<?= isset($data['idkategori']) ? $data['idkategori'] : '' ?>"
               <?= isset($data['idkategori']) ? 'readonly' : '' ?> required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Nama Kategori</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="namakategori" placeholder="Masukkan Nama Kategori" value="<?= isset($data['namakategori']) ? $data['namakategori'] : '' ?>" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"> Simpan</span></button>
            <a href="<?= site_url() ?>/kategori" class="btn btn-danger"><span class="glyphicon glyphicon-remove"> Batal</span></a>
        </div>
    </div>  
</form>
