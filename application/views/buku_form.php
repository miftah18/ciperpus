<form class="form-horizontal" action="<?= site_url()?>/buku/<?= isset($data['kdbuku']) ? 'update' : 'save' ?>">
    <div class="form-group">
        <label class="col-sm-2 control-label">Kode Buku</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="kdbuku" placeholder="Masukkan Kode Buku" value="<?= isset($data['kdbuku']) ? $data['kdbuku'] : '' ?>"
               <?= isset($data['kdbuku']) ? 'readonly' : '' ?> required>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 control-label">Judul Buku</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="judulbuku" placeholder="Masukkan Judul Buku" value="<?= isset($data['judulbuku']) ? $data['judulbuku'] : '' ?>" required>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 control-label">Pengarang</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="pengarang" placeholder="Masukkan Nama Pengarang Buku" value="<?= isset($data['pengarang']) ? $data['pengarang'] : '' ?>" required>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 control-label">Penerbit</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="penerbit" placeholder="Masukkan Nama Penerbit Buku" value="<?= isset($data['penerbit']) ? $data['penerbit'] : '' ?>" required>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 control-label">Tahun Terbit</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="tahunterbit" placeholder="Masukkan Tahun Terbit Buku" value="<?= isset($data['tahunterbit']) ? $data['tahunterbit'] : '' ?>" required>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 control-label">Id Kategori</label>
        <div class="col-sm-6">
		<input type="hidden" name="kdbuku" 
		value="<?= isset($data['kdbuku']) ? $data['kdbuku'] : '' ?>" >
            <select name="idkategori" class="form-control">
                <?php foreach ($kategori as $r) { 
				$pilih=($data['idkategori']==$r['idkategori'])? 'selected' : '';
				?>
                <option value="<?= $r['idkategori'] ?>" <?=$pilih ?>>
                    <?= $r['namakategori'] ?>
                </option>
                <?php } ?>
            </select>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 control-label">Stok</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="stok" placeholder="Masukkan Stok Buku" value="<?= isset($data['stok']) ? $data['stok'] : '' ?>" required>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 control-label">Tanggal Masuk</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="tglmasuk" placeholder="Masukkan Tanggal Masuk Buku" value="<?= isset($data['tglmasuk']) ? $data['tglmasuk'] : '' ?>" required>
        </div>
    </div>
    
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"> <?= isset($data['kdbuku']) ? 'Ubah' : 'Simpan'</span></button>
            <a href="<?= site_url() ?>/buku" class="btn btn-danger"><span class="glyphicon glyphicon-remove"> Batal</span></a>
        </div>
    </div>  
</form>
