<a href="<?= site_url() ?>/kategori/form" class="btn btn-success"><span class="glyphicon glyphicon-plus"> Tambah Data</span></a><br /><br />
<table class="table table-striped table-bordered table-hover table-condensed">
    <thead style="background-color: #428bca">
        <tr style="color: white">
            <th><center>Id Kategori</center></th>
            <th><center>Nama Kategori</center></th>
            <th><center>Action</center></th>
        </tr>
    </thead>
    <tbody>
        <?php $no =0; foreach ($kategori as $r) { ?>
        <tr>
            <td align="center"><?= $r['idkategori']  ?></td>
            <td><?= $r['namakategori']  ?></td>
            <td align="center">
                <a href="<?= site_url('kategori/edit/'.$r['idkategori']) ?>" class="btn btn-primary"><span class="glyphicon glyphicon-edit" title="Edit Data"></span></a>
                <a href="#" rel="<?= site_url('kategori/delete/'.$r['idkategori']) ?>" class="del btn btn-warning"><span class="glyphicon glyphicon-trash" title="Hapus Data"></span></a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>