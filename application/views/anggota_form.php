<form class="form-horizontal" method="post" action="<?= site_url() ?>/anggota/<?= isset($data['kdanggota']) ? 'update' : 'save' ?>">
    <div class="form-group">
        <label class="col-sm-2 control-label">Kode Anggota</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="kdanggota" placeholder="Masukkan Kode Anggota Anda" value="<?= isset($data['kdanggota']) ? $data['kdanggota'] : '' ?>"
               <?= isset($data['kdanggota']) ? 'readonly' : '' ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Nama</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="nama_anggota" placeholder="Masukkan Nama Lengkap Anda" value="<?= isset($data['nama_anggota']) ? $data['nama_anggota'] : '' ?>" required>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">No. Handphone</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="nohp" placeholder="Masukkan No. Handphone Anda" value="<?= isset($data['nohp']) ? $data['nohp'] : '' ?>" required>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-2 control-label">Alamat</label>
        <div class="col-sm-6">
            <input type="text" class="form-control" name="alamat" placeholder="Masukkan Alamat Lengkap Anda" value="<?= isset($data['alamat']) ? $data['alamat'] : '' ?>" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-floppy-disk"> Simpan</span></button>
            <a href="<?= site_url() ?>/anggota" class="btn btn-danger"><span class="glyphicon glyphicon-remove"> Batal</span></a>
        </div>
    </div>  
</form>
