<a href="<?= site_url() ?>/buku/form" class="btn btn-success"><span class="glyphicon glyphicon-plus"> Tambah Data</span></a><br /><br />
<table class="table table-striped table-bordered table-hover table-condensed">
    <thead style="background-color: #428bca">
        <tr style="color: white">
            <th><center>Kode Buku</center></th>
            <th><center>Judul Buku</center></th>
            <th><center>Pengarang</center></th>
            <th><center>Penerbit</center></th>
            <th><center>Tahun Terbit</center></th>
            <th><center>Id Kategori</center></th>
            <th><center>Stok</center></th>
            <th><center>Tanggal Masuk</center></th>
			<th><center>Action</center></th>
        </tr>
    </thead>
    <tbody>
        <?php $no =0; foreach ($buku as $r) { ?>
        <tr>
            <td align="center"><?= $r['kdbuku']  ?></td>
            <td align="center"><?= $r['judulbuku']  ?></td>
			<td align="center"><?= $r['pengarang']  ?></td>
            <td align="center"><?= $r['penerbit']  ?></td>
            <td align="center"><?= $r['tahunterbit']  ?></td>
			<td><?= $r['kategori']  ?></td>
            <td align="center"><?= $r['stok']  ?></td>
            <td align="center"><?= $r['tglmasuk']  ?></td>
            <td align="center">
                <a href="<?= site_url('buku/edit/'.$r['kdbuku']) ?>" class="btn btn-primary"><span class="glyphicon glyphicon-edit" title="Edit Data"></span></a>
                <a href="#" rel="<?= site_url('buku/delete/'.$r['kdbuku']) ?>" class="del btn btn-warning"><span class="glyphicon glyphicon-trash" title="Hapus Data"></span></a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>